package com.example.telematics;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;

import com.example.ds_app.model.Message;


public class MainActivity extends AppCompatActivity {

    private static Socket s;
    private ObjectOutputStream out;
    private ObjectInputStream in;
   private final AtomicInteger msgIDCounter = new AtomicInteger(0);

    private static String ip = "10.0.2.2";





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        final CardView cardView = findViewById(R.id.mainCard);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myTask mt = new myTask();
                mt.execute();

                Intent mySecondActivityIntend = new Intent(v.getContext(), Main3Activity.class);
                startActivityForResult(mySecondActivityIntend, 0);
            }
        });


    }
    class myTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                s= new Socket(InetAddress.getByName(ip), 4333);
                out = new ObjectOutputStream(s.getOutputStream());
                in = new ObjectInputStream(s.getInputStream());

                Message msg = new Message("SUB", ip + ":" + "4333", "SUB1", msgIDCounter.getAndIncrement() + "",
                        "Hi", "", null);

                out.writeUnshared(msg);
                out.flush();

            } catch (UnknownHostException e){
                Log.e("tag1", "You are trying to connect to an unknown host!");
            }catch (ConnectException ce) {
                Log.e("tag2", "Node unable to connect");
            } catch (IOException ioE) {
                Log.e("tag3", "IO error");
            }



            return null;
        }
    }
}
