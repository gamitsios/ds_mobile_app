package com.example.telematics;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);

        final Button apply = (Button) findViewById(R.id.applyButton);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final TextView tv = (TextView) findViewById(R.id.editText);
                androidClick(v,tv);
                tv.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode ==KeyEvent.KEYCODE_ENTER){
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(tv.getWindowToken(),0);
                            androidClick(v,tv);
                        }
                        return false;
                    }
                });
            }

        });
        final Button cancel = (Button) findViewById(R.id.cancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel.setBackgroundResource(R.color.red);
                Intent goBackActivityIntent = new Intent(v.getContext(),MainActivity.class);
                startActivityForResult(goBackActivityIntent,0);
            }
        });

    }
    private void androidClick(View view, TextView tv){
        String value = tv.getText().toString().trim();
        tv.setText(value);
        if (value.equals("")){
            Toast.makeText(getApplicationContext(), "Value is missing!", Toast.LENGTH_SHORT).show();

        } else{
            Toast.makeText(getApplicationContext(), "Applied " + value, Toast.LENGTH_SHORT).show();
            Intent showBusInfoIntent = new Intent(view.getContext(), Main3Activity.class);
            startActivityForResult(showBusInfoIntent, 0);


        }


    }




}
