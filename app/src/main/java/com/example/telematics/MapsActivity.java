package com.example.telematics;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


import com.example.ds_app.FileLoader;
import com.example.ds_app.model.Bus;
import com.example.ds_app.model.BusRoute;
import com.example.ds_app.model.InitialList;
import com.example.ds_app.model.Topic;
import com.example.ds_app.model.Value;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.IOException;



import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private Intent intent;
    private FileLoader loader = new FileLoader();
    private Location busLastLocation;
    private Marker currentPositionMarker;
    // LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        intent = getIntent();
        InitialList list = (InitialList) intent.getSerializableExtra("message");

        getSupportActionBar().setTitle(list.toString());
        ArrayList<BusRoute> busRoutes = null;
        try {
            busRoutes = loader.loadBusRoutes(getAssets().open("RouteCodesNew.txt"), getAssets().open("busLinesNew.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<InitialList> myMenu = new ArrayList<InitialList>();
        Topic selectedTopic = list.getTopic();
        for (BusRoute bs : busRoutes) {
            if (selectedTopic.toString().equals(bs.getBusLineID())) {
                InitialList myList = new InitialList();
                myList.setTopic(selectedTopic);
                myList.setInfo(bs.getLineName());
                myMenu.add(myList);
            }
        }
        ListView listView = (ListView) findViewById(R.id.menuList);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, myMenu);
        listView.setAdapter(arrayAdapter);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            //ends the activity
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Bus bus = new Bus("1151", "2484", "10389", "PLATEIA KANIGKOS - GKIZI", "021", null);
        final ArrayList<Value> values = new ArrayList<Value>();
        Value val1 = new Value(bus, 37.982891, 23.73476);
        values.add(val1);
        Value val2 = new Value(bus, 37.993143, 23.749235);
        values.add(val2);
        Value val3 = new Value(bus, 37.994142, 23.759235);
        values.add(val3);
        Value val4 = new Value(bus, 37.995143, 23.769235);
        values.add(val4);

        currentPositionMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(val1.getLatitude(), val1.getLongitude()))
                .title("Bus")
                .snippet("Current " + val1.getLatitude() + " " + val1.getLongitude())
                .icon(bitmapDescriptorFromVector(this, R.drawable.direction_bus)).flat(true));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(val1.getLatitude(), val1.getLongitude()), 12));


        moveTheBusTo(val2,3000);
        moveTheBusTo(val3, 4000);
        moveTheBusTo(val4, 5000);


        // animateMarker(m, busPosition );
        // m.setPosition(new LatLng(40, 40));
        //   animateMarker(m, busPosition,true);

      /*  LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (Marker marker : mMarkers){
           builder.include(marker.getPosition());
        } */


    }
    private void moveTheBusTo (final Value v, int delayTimer){
        long start = SystemClock.uptimeMillis();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                    if (currentPositionMarker != null){
                        currentPositionMarker.remove();

                    }
                    currentPositionMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(v.getLatitude(), v.getLongitude()))
                            .title("Bus")
                            .snippet("Current " + v.getLatitude() + " " + v.getLongitude())
                            .icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.direction_bus)).flat(true));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(v.getLatitude(), v.getLongitude()), 12));
                    Toast.makeText(getApplicationContext(), "Location Changed to: " + v.getLatitude(), Toast.LENGTH_SHORT).show();


            }
        },delayTimer);
    }

    /*  this method converts an xml vector to a bitmap so the marker can use it
     */
    private static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private static void animateMarker(final GoogleMap myMap, final   Marker marker, final Value value,
                                      final boolean hideMarker, final Context context) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = myMap.getProjection();
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {

            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                    if (marker != null){
                        marker.remove();

                    }

                  Marker newMarker=  myMap.addMarker(new MarkerOptions().position( new LatLng(value.getLatitude(), value.getLongitude()))
                            .title("Bus")
                            .snippet("Current " + value.getLatitude() + " " + value.getLongitude())
                            .icon(bitmapDescriptorFromVector(context, R.drawable.direction_bus)).flat(true));

                    myMap.animateCamera(CameraUpdateFactory.newLatLngZoom( new LatLng(value.getLatitude(), value.getLongitude()), 12));
                    //Toast.makeText(getApplicationContext(), "moved to: "+ v.getLatitude(), Toast.LENGTH_SHORT).show();



                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 150);
                } else {
                    if (hideMarker) {
                        newMarker.setVisible(false);
                    } else {
                        newMarker.setVisible(true);
                    }
                }

            }
        });

    }


 /*   @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(this, "Location Changed " + location.getLatitude()
                + location.getLongitude(), Toast.LENGTH_LONG).show();
        busLastLocation = location;
        if (currentPositionMarker != null){
            currentPositionMarker.remove();
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        currentPositionMarker = mMap.addMarker(new MarkerOptions().position(latLng)
                .title("Bus")
                .snippet("Current"+ location.getLatitude() + " " + location.getLongitude())
                .icon(bitmapDescriptorFromVector(this, R.drawable.direction_bus)));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(11));



    } */

}
