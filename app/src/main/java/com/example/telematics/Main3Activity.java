package com.example.telematics;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.ds_app.FileLoader;
import com.example.ds_app.model.BusRoute;
import com.example.ds_app.model.InitialList;
import com.example.ds_app.model.Topic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

public class Main3Activity extends AppCompatActivity {

    private FileLoader loader = new FileLoader();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);


        Toolbar toolbar = findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
       getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView listView = (ListView) findViewById(R.id.listView);
         ArrayList<InitialList> myList = new ArrayList<InitialList>();
        try {
            myList = loader.loadTopics(getAssets().open("busLinesNew.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }


        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,myList);
        listView.setAdapter(arrayAdapter);
        final ArrayList<InitialList> finalMyList = myList;
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
                //Toast.makeText(getApplicationContext(), "Chose busLine: "+ finalMyList.get(i),Toast.LENGTH_SHORT).show();
                Intent googleMapsIntent = new Intent(view.getContext(),MapsActivity.class);
                googleMapsIntent.putExtra("message", finalMyList.get(i));
                startActivityForResult(googleMapsIntent, 0);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if (id == android.R.id.home){
            //ends the activity
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }






}
