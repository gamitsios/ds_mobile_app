package com.example.ds_app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;


import com.example.ds_app.model.Bus;
import com.example.ds_app.model.BusRoute;
import com.example.ds_app.model.Message;
import com.example.ds_app.model.Topic;
import com.example.ds_app.model.Value;
import com.sun.corba.se.pept.broker.Broker;

import static jdk.nashorn.internal.objects.NativeArray.push;


/**
 * A subscriber node. Connect to a broker and visualize the data that receives.
 *
 */
public class SubscriberNode implements Subscriber {

	// Create logger
	//private static final Logger logger = LogManager.getLogger(SubscriberNode.class.getName());
	/**
	 * the ID of the subscriber
	 */
	private String subID;
	private Socket requestSocket;
	private ObjectOutputStream out;
	private ObjectInputStream in;

	/**
	 * The IP of the broker node
	 */
	private final String brokerIP;

	/**
	 * the port of the broker node
	 */
	private final int brokerPort;

	/**
	 * Atomic variable to keep the msgID counter thread safe
	 */
	private final AtomicInteger msgIDCounter;

	/**
	 * The topic of the subscriber
	 */
	private final Topic topic;

	/**
	 * Creates a new subscriber node
	 * 
	 * @param subID the id of the subscriber
	 */
	public SubscriberNode(String subID, String brokerIP, int brokerPort, Topic topic) {
		super();
		this.subID = subID;
		this.msgIDCounter = new AtomicInteger(0);
		this.brokerIP = brokerIP;
		this.brokerPort = brokerPort;
		this.topic = topic;

	}

	@Override
	public void init(int num) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connect() {
		try {
			requestSocket = new Socket(InetAddress.getByName(brokerIP), brokerPort);
			out = new ObjectOutputStream(requestSocket.getOutputStream());
			in = new ObjectInputStream(requestSocket.getInputStream());

			System.out.println("Subscriber : " + subID + " connected to " + brokerIP + ":" + brokerPort);
			Message msg = new Message("SUB", brokerIP + ":" + brokerPort, subID, msgIDCounter.getAndIncrement() + "",
					"Hi", "", null);

			out.writeUnshared(msg);
			out.flush();

			register(null, topic);

			while (true) {

				msg = ((Message) in.readUnshared());

				visualiseData(new Topic(msg.getTopic()), msg.getValue());
			}

		} catch (UnknownHostException unknownHost) {
			System.out.println(("You are trying to connect to an unknown host!"));
		} catch (ConnectException ce) {
			//logger.error("Node unable to connect: " + ce.getMessage());
		} catch (IOException ioE) {
		//	logger.error("IO error ! " + ioE.getMessage());
		} catch (ClassNotFoundException e) {
		//	logger.error(e.getMessage());
		}

	}

	@Override
	public void disconnect() {
		Message msg = null;

		try {
			// send a notification bye message to node
			msg = new Message("SUB", requestSocket.getPort() + "", subID, msgIDCounter.getAndIncrement() + "", "bye",
					"", null);
			out.writeUnshared(msg);
			out.flush();

			// wait some time for the notification to arrive
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				//logger.warn("Wait time for publisher to disconnect interrupted." + e.getMessage());
			}

			// close streams and socket
			in.close();
			out.close();
			requestSocket.close();
		} catch (IOException ioException) {
			//logger.error("Unable to close socket connection.", ioException);
		}
		//logger.info("Subscriber id: " + subID + " disconnected.");

	}

	@Override
	public void updateNodes() {
		// TODO Auto-generated method stub

	}

	@Override
	public void register(Broker broker, Topic topic) {
		Message msg = new Message("SUB", requestSocket.getPort() + "", subID, msgIDCounter.getAndIncrement() + "",
				"REGISTER", topic.getBusLine(), null);
		try {
			out.writeUnshared(msg);
			out.flush();
		} catch (IOException ioE) {
			//logger.error("IO error ! Could not push a message. ", ioE);
		}

	}

	@Override
	public void disconnect(Broker broker, Topic topic) {
		// TODO Auto-generated method stub

	}

	@Override
	public void visualiseData(Topic topic, Value v) {
		System.out.println("Subscriber Msg received: Topic - " + topic.getBusLine() + ", " + v.toString());

	}


	public static void main(String[] args) {
		String subID = "SUB1";
		String brokerIP = "127.0.0.1";
		int brokerPort = 4333;
		Topic topic = new Topic("022");
		SubscriberNode node1 = new SubscriberNode(subID, brokerIP, brokerPort, topic);
		node1.connect();


	}

}
