package com.example.ds_app;

public interface Node {
	
	void init(int num);
	void connect();
	void disconnect();
	void updateNodes();
	

}
