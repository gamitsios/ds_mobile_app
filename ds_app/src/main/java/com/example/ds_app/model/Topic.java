package com.example.ds_app.model;

import java.io.Serializable;

public class Topic implements Serializable{
	private static final long serialVersionUID = -9214237926326844588L;
	private String busLine;

	/**
	 * @param busLine
	 */
	public Topic(String busLine) {
		super();
		this.busLine = busLine;
	}

	/**
	 * @return the busLine
	 */
	public String getBusLine() {
		return busLine;
	}

	/**
	 * @param busLine the busLine to set
	 */
	public void setBusLine(String busLine) {
		this.busLine = busLine;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return  busLine ;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((busLine == null) ? 0 : busLine.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Topic other = (Topic) obj;
		if (busLine == null) {
			if (other.busLine != null)
				return false;
		} else if (!busLine.equals(other.busLine))
			return false;
		return true;
	}
	
	
	
	
	
	

}
