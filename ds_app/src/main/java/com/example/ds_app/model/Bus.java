package com.example.ds_app.model;

import java.io.Serializable;

/**
 * Class that represents a Bus object 
 *
 */
public class Bus implements Serializable {
	private static final long serialVersionUID = 4777921434776941751L;
	private String lineNumber;
	private String routeCode;
	private String vehicleId;	
	private String lineName;
	private String buslineId;
	private String info;
	
	public Bus() {
		
	}
	public Bus(String lineNumber, String routeCode, String vehicleId, String lineName, String buslineId, String info) {
		super();
		this.lineNumber = lineNumber;
		this.routeCode = routeCode;
		this.vehicleId = vehicleId;
		this.lineName = lineName;
		this.buslineId = buslineId;
		this.info = info;
	}

	/**
	 * Gets the lineNumber 
	 * @return the lineNumber
	 */
	public String getLineNumber() {
		return lineNumber;
	}

	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}

	/**
	 * @return the routeCode
	 */
	public String getRouteCode() {
		return routeCode;
	}

	/**
	 * @param routeCode the routeCode to set
	 */
	public void setRouteCode(String routeCode) {
		this.routeCode = routeCode;
	}

	/**
	 * @return the vehicleId
	 */
	public String getVehicleId() {
		return vehicleId;
	}

	/**
	 * @param vehicleId the vehicleId to set
	 */
	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	/**
	 * @return the lineName
	 */
	public String getLineName() {
		return lineName;
	}

	/**
	 * @param lineName the lineName to set
	 */
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}

	/**
	 * @return the buslineId
	 */
	public String getBuslineId() {
		return buslineId;
	}

	/**
	 * @param buslineId the buslineId to set
	 */
	public void setBuslineId(String buslineId) {
		this.buslineId = buslineId;
	}

	/**
	 * @return the info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * @param info the info to set
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Bus [lineNumber=" + lineNumber + ", routeCode=" + routeCode + ", vehicleId=" + vehicleId + ", lineName="
				+ lineName + ", buslineId=" + buslineId + ", info=" + info + "]";
	}
	
	
	

}
