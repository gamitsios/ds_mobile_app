package com.example.ds_app.model;



import java.io.Serializable;

public class Value implements Serializable{
	private static final long serialVersionUID = 5704318631023996282L;
	private Bus bus;
	private double latitude;
	private double longitude;
	/**
	 * @param bus
	 * @param latitude
	 * @param longtitude
	 */
	public Value(Bus bus, double latitude, double longtitude) {
		super();
		this.bus = bus;
		this.latitude = latitude;
		this.longitude = longtitude;
	}
	/**
	 * @return the bus
	 */
	public Bus getBus() {
		return bus;
	}
	/**
	 * @param bus the bus to set
	 */
	public void setBus(Bus bus) {
		this.bus = bus;
	}
	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longtitude
	 */
	public double getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Value [bus=" + bus + ", latitude=" + latitude + ", longitude=" + longitude + "]";
	}
	
	
	

}
