package com.example.ds_app.model;

import java.io.Serializable;

/**
 * Saves information about broker nodes
 *
 */
public class BrokerInfo implements Comparable<BrokerInfo> ,Serializable{
	
	private static final long serialVersionUID = 4103274402327783174L;
	private String brokerID;
	private String brokerIP;
	private int port;
	private String key;
	/**
	 * @param brokerID
	 * @param brokerIP
	 * @param port
	 * @param key
	 */
	public BrokerInfo(String brokerID, String brokerIP, int port, String key) {
		super();
		this.brokerID = brokerID;
		this.brokerIP = brokerIP;
		this.port = port;
		this.key = key;
	}
	/**
	 * @return the brokerID
	 */
	public String getBrokerID() {
		return brokerID;
	}
	/**
	 * @param brokerID the brokerID to set
	 */
	public void setBrokerID(String brokerID) {
		this.brokerID = brokerID;
	}
	/**
	 * @return the brokerIP
	 */
	public String getBrokerIP() {
		return brokerIP;
	}
	/**
	 * @param brokerIP the brokerIP to set
	 */
	public void setBrokerIP(String brokerIP) {
		this.brokerIP = brokerIP;
	}
	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
	
	
	@Override
	public int compareTo(BrokerInfo o) {
		return this.getKey().compareTo(o.getKey());
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brokerID == null) ? 0 : brokerID.hashCode());
		result = prime * result + ((brokerIP == null) ? 0 : brokerIP.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + port;
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BrokerInfo other = (BrokerInfo) obj;
		if (brokerID == null) {
			if (other.brokerID != null)
				return false;
		} else if (!brokerID.equals(other.brokerID))
			return false;
		if (brokerIP == null) {
			if (other.brokerIP != null)
				return false;
		} else if (!brokerIP.equals(other.brokerIP))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (port != other.port)
			return false;
		return true;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BrokerInfo [brokerID=" + brokerID + ", brokerIP=" + brokerIP + ", port=" + port + ", key=" + key + "]";
	}

	
	

}
