package com.example.ds_app.model;

import java.io.Serializable;
import java.util.Objects;

public class InitialList implements Serializable {
    private Topic topic;
    private String info;

    public InitialList(Topic topic, String info) {
        super();
        this.topic = topic;
        this.info = info;
    }

    public InitialList() {
        super();
    }

    public Topic getTopic() {
        return topic;
    }

    public String getInfo() {
        return info;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    public String toString(){
        return topic + " " + info;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InitialList list = (InitialList) o;
        return Objects.equals(topic, list.topic) &&
                Objects.equals(info, list.info);
    }

    @Override
    public int hashCode() {
        return Objects.hash(topic, info);
    }
}

