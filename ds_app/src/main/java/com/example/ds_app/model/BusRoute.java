package com.example.ds_app.model;

public class BusRoute {
	private String lineCode;
	private String busLineID;
	private String RouteCode;
	private String lineName;
	
	
	
	public BusRoute(){
		
	}
	public BusRoute(String lineCode, String busLineID, String lineName,
			String routeCode) {
		super();
		this.lineCode = lineCode;
		this.busLineID = busLineID;
		this.lineName = lineName;
		RouteCode = routeCode;
	}
	public String getLineCode() {
		return lineCode;
	}
	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}
	public String getBusLineID() {
		return busLineID;
	}
	public void setBusLineID(String busLineID) {
		this.busLineID = busLineID;
	}
	public String getLineName() {
		return lineName;
	}
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}
	public String getRouteCode() {
		return RouteCode;
	}
	public void setRouteCode(String routeCode) {
		RouteCode = routeCode;
	}
	@Override
	public String toString() {
		return "BusRoute [lineCode=" + lineCode + ", busLineID=" + busLineID
				+ ", RouteCode=" + RouteCode + ", lineName=" + lineName + "]";
	}



	
}
