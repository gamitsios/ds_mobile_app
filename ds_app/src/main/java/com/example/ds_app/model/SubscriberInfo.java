package com.example.ds_app.model;

import java.io.ObjectOutputStream;

public class SubscriberInfo {
	
	private String nodeID;
	private ObjectOutputStream outStream;
	/**
	 * @param nodeID
	 * @param outStream
	 */
	public SubscriberInfo(String nodeID, ObjectOutputStream outStream) {
		super();
		this.nodeID = nodeID;
		this.outStream = outStream;
	}
	/**
	 * @return the nodeID
	 */
	public String getNodeID() {
		return nodeID;
	}
	/**
	 * @param nodeID the nodeID to set
	 */
	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}
	/**
	 * @return the outStream
	 */
	public ObjectOutputStream getOutStream() {
		return outStream;
	}
	/**
	 * @param outStream the outStream to set
	 */
	public void setOutStream(ObjectOutputStream outStream) {
		this.outStream = outStream;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeID == null) ? 0 : nodeID.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscriberInfo other = (SubscriberInfo) obj;
		if (nodeID == null) {
			if (other.nodeID != null)
				return false;
		} else if (!nodeID.equals(other.nodeID))
			return false;
		return true;
	}
	
	

}
