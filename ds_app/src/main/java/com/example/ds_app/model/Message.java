package com.example.ds_app.model;

import java.io.Serializable;

public class Message  implements Serializable{
	
	private static final long  serialVersionUID = 1406654642558450465L;
	
	private String from;
	
	private String to;
	
	private String senderID;
	
	private String msgId;
	
	private String msgType;
	
	private String topic;
	
	private Value value;

	/**
	 * @param from
	 * @param to
	 * @param senderID
	 * @param msgId
	 * @param msgType
	 * @param topic
	 */
	public Message(String from, String to, String senderID, String msgId, String msgType, String topic, Value value) {
		super();
		this.from = from;
		this.to = to;
		this.senderID = senderID;
		this.msgId = msgId;
		this.msgType = msgType;
		this.topic = topic;
		this.value = value;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the senderID
	 */
	public String getSenderID() {
		return senderID;
	}

	/**
	 * @param senderID the senderID to set
	 */
	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	/**
	 * @return the value
	 */
	public Value getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Value value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Message [from=" + from + ", to=" + to + ", senderID=" + senderID + ", msgId=" + msgId + ", msgType="
				+ msgType + ", topic=" + topic + ", value=" + value + "]";
	}



	
	
	
	
}
