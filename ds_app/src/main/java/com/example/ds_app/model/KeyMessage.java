package com.example.ds_app.model;

import com.example.ds_app.model.Topic;

import java.io.Serializable;
import java.util.List;

public class KeyMessage  implements Serializable{
	
	private static final long serialVersionUID = 8706738153867303168L;

	private String from;
	
	private String to;
	
	private String senderID;
	
	private String msgId;
	
	private String msgType;
	
	private BrokerInfo broker;
	
	private List<Topic> topics;

	/**
	 * @param from
	 * @param to
	 * @param senderID
	 * @param msgId
	 * @param msgType
	 * @param broker
	 * @param topics
	 */
	public KeyMessage(String from, String to, String senderID, String msgId, String msgType, BrokerInfo broker,
			List<Topic> topics) {
		super();
		this.from = from;
		this.to = to;
		this.senderID = senderID;
		this.msgId = msgId;
		this.msgType = msgType;
		this.broker = broker;
		this.topics = topics;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @param from the from to set
	 */
	public void setFrom(String from) {
		this.from = from;
	}

	/**
	 * @return the to
	 */
	public String getTo() {
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to) {
		this.to = to;
	}

	/**
	 * @return the senderID
	 */
	public String getSenderID() {
		return senderID;
	}

	/**
	 * @param senderID the senderID to set
	 */
	public void setSenderID(String senderID) {
		this.senderID = senderID;
	}

	/**
	 * @return the msgId
	 */
	public String getMsgId() {
		return msgId;
	}

	/**
	 * @param msgId the msgId to set
	 */
	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	/**
	 * @return the msgType
	 */
	public String getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	/**
	 * @return the broker
	 */
	public BrokerInfo getBroker() {
		return broker;
	}

	/**
	 * @param broker the broker to set
	 */
	public void setBroker(BrokerInfo broker) {
		this.broker = broker;
	}

	/**
	 * @return the topics
	 */
	public List<Topic> getTopics() {
		return topics;
	}

	/**
	 * @param topics the topics to set
	 */
	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}

	



	
	
	
	
}
