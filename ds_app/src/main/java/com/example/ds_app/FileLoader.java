package com.example.ds_app;

import com.example.ds_app.model.BusRoute;
import com.example.ds_app.model.InitialList;
import com.example.ds_app.model.Topic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class FileLoader {


    public ArrayList<InitialList> loadTopics(InputStream topicsFile) {
        BufferedReader reader = null;
        LinkedHashSet<InitialList> hashedList = new LinkedHashSet<InitialList>();
        try {
            reader = new BufferedReader(new InputStreamReader(topicsFile));
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] lineArray = line.split(",");
                Topic myTopic = new Topic(lineArray[1]);
                InitialList init = new InitialList();
                init.setTopic(myTopic);
                init.setInfo(lineArray[2]);
                hashedList.add(init);
            }




        } catch (FileNotFoundException e) {
            e.getStackTrace();
        } catch (IOException ioe) {
            ioe.getStackTrace();
        } finally {
            try {
                reader.close();
                reader.close();
            } catch (IOException e) {
                e.getStackTrace();
            }

        }

        ArrayList<InitialList> initialList = new ArrayList<>(hashedList);
        return initialList;
    }

    public ArrayList<BusRoute> loadBusRoutes(InputStream routesFile, InputStream topicsFile) {
        BufferedReader routesReader = null;
        BufferedReader topicsReader = null;
        ArrayList<BusRoute> BusRoutes = new ArrayList<BusRoute>();


        try {
            routesReader = new BufferedReader(new InputStreamReader(routesFile));
            topicsReader = new BufferedReader(new InputStreamReader(topicsFile));
            String line = null;

            while ((line = routesReader.readLine()) != null) {
                BusRoute BusRoute = new BusRoute();
                String[] routeLineArray = line.split(",");
                BusRoute.setRouteCode(routeLineArray[0]);
                BusRoute.setLineCode(routeLineArray[1]);
                BusRoute.setLineName(routeLineArray[3]);
                BusRoutes.add(BusRoute);
            }


            line = null;
            while ((line = topicsReader.readLine()) != null) {
                String[] topicsLineArray = line.split(",");


                for (BusRoute bs : BusRoutes) {
                    if (topicsLineArray[0].equals(bs.getLineCode())) {
                        bs.setBusLineID(topicsLineArray[1]);
                        bs.setBusLineID(topicsLineArray[1]);
                    }
                }


            }

        } catch (FileNotFoundException e) {
            e.getStackTrace();
        } catch (IOException ioe) {
            ioe.getStackTrace();
        } finally {
            try {
                routesReader.close();
                topicsReader.close();
            } catch (IOException e) {
                e.getStackTrace();
            }

        }
        return BusRoutes;
    }
}
