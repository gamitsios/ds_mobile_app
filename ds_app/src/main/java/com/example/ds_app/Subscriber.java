package com.example.ds_app;

import com.example.ds_app.model.Topic;
import com.example.ds_app.model.Value;
import com.sun.corba.se.pept.broker.Broker;

public interface Subscriber extends Node {
	void register(Broker broker, Topic topic);
	void disconnect(Broker broker, Topic topic);
	void visualiseData(Topic topic, Value v);
	

}
